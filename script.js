document.querySelector('.main-our-services-item').classList.add('active');
document.querySelector('.main-our-services-text-item').classList.add('active');

function selectContent (e) {
  let target = e.target.dataset.ourServicesTabName;
  document.querySelectorAll('.main-our-services-item, .main-our-services-text-item').forEach(el => el.classList.remove('active'));
  e.target.classList.add('active');
  document.querySelector('.' + target).classList.add('active');
}

document.querySelectorAll('.main-our-services-item').forEach(el => {
  el.addEventListener('click', selectContent);
})


// our work
let menu = document.querySelector('.main-our-work-menu');
let items = document.querySelectorAll('.our-work-tabs-item');
let menuTitles = document.querySelectorAll('.main-our-work-item');

function filterPictures (e) {
  let targetOurWorkTabName = e.target.getAttribute('data-our-work-tab-name');
  if (targetOurWorkTabName) {
    let targetTitle = e.target;
    menuTitles.forEach(menuTitle => menuTitle.classList.remove('active'));
    targetTitle.classList.add('active');
  }
  if (targetOurWorkTabName === 'all') {
    items.forEach(item => {
      item.style.display = 'block';
    })
  } else {
    items.forEach(item => {
      if (item.getAttribute('data-our-work-tab-name') === targetOurWorkTabName) {
        item.style.display = 'block';
      } else {
        item.style.display = 'none';
      }
    })
  }
}
menu.addEventListener('click', filterPictures);

document.querySelector('.main-our-work-btn').addEventListener('click', event => {
document.querySelector('.main-our-work-btn p').style.display = 'none';
document.querySelector('.main-our-work-btn span').style.display = 'flex';
setTimeout(loadMore, 2000);
})

function loadMore () {
  let btn = document.querySelector('.main-our-work-btn');
  let hiddenContent = document.querySelector('.tabs-items-load');
  btn.style.display = 'none';
  hiddenContent.classList.add('active');
}

//people say

let currentSlide = 0;
let next = document.querySelector('.main-people-say-slider-btn-right');
let previous = document.querySelector('.main-people-say-slider-btn-left');
let navigation = document.querySelectorAll('.main-people-say-slider-photo');
let users = document.querySelectorAll('.main-people-say-user');

function selectReview (e) {
  let target = e.target.closest("li");
  let dataArr = e.target.closest("li").getAttribute("data-people-say-name");

  document.querySelectorAll(".main-people-say-slider-photo, .main-people-say-user").forEach(el => {el.classList.remove("active");
  })

  target.classList.add("active");
  users.forEach((review, i) => {
    if (review.getAttribute("data-people-say-name") === dataArr) {
      currentSlide = i;
      review.classList.add("active");
    } else {
      review.classList.remove("active");
    }
  })
}

navigation.forEach(el => {
  el.addEventListener("click", selectReview);
})

previous.addEventListener("click", () => {
  if (currentSlide === 0) {
    currentSlide = users.length - 1;
  } else {
    currentSlide--;
  }
  users.forEach((review, i) => {
    review.classList.remove("active");
  });
  users[currentSlide].classList.add("active");
  navigation.forEach((element, i) => {
    element.classList.remove("active");
  });
  navigation[currentSlide].classList.add("active");
})

next.addEventListener("click", () => {
  if(currentSlide === users.length - 1) {
    currentSlide = 0;
  } else {
    currentSlide++
  }
  users.forEach((review, i) => {
    review.classList.remove("active");
  });
  users[currentSlide].classList.add("active");
  navigation.forEach((element, i) => {
    element.classList.remove("active");
  });
  navigation[currentSlide].classList.add("active");
})




